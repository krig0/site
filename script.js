function hideAll(){
	document.getElementById('dvd-field').style.display = 'none';
	document.getElementById('book-field').style.display = 'none';
	document.getElementById('furniture-field').style.display = 'none';
	document.getElementById('descript').style.display = 'none';
}
function showSwitched(elementId){
	var arr = { "dvd": "size", "book": "weight", "furniture": "dimension HxWxL"}; 
	document.getElementById(elementId + '-field').style.display = 'block';
	document.getElementById('descript').style.display = 'block';
	document.getElementById('descript').textContent = "Please, provide " + arr[elementId];
}
function showText(value){
	hideAll();
	showSwitched(value);
}
hideAll();

function checkField(fieldName, fieldType){
	let field = document.forms["productForm"][fieldName].value;
	if(field != ""){
		if(isNaN(field) && fieldType == "string") return true;
		if(!isNaN(field) && fieldType == "number") return true;
		document.getElementById('alert').textContent = "Please, provide the data of indicated type in " + fieldName; 
		return false;
	}
	document.getElementById('alert').textContent = "Please, submit " + fieldName;
	return false;
}
function checkChoosen(){
	const productType = document.forms["productForm"]["param_name"].value;
	switch(productType){
		case "dvd": return checkField('size',"number");
		case "book": return checkField('weight',"number");
		case "furniture": return checkField('height',"number") && checkField('width',"number") && checkField('length',"number");
		default: document.getElementById('alert').textContent = "type not choosen"; return false;
	}
}
function checkEmpty(){
	return checkField('sku',"string")
	&& checkField('name',"string")
	&& checkField('price',"number")
	&& checkChoosen();
}

function formRedirect(){
	document.getElementById("product_form").action = "productSave.php";
	return checkEmpty();
}