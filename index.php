<html>
<head>
	<meta charset="utf-8">
	<title>DreamPipe</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div id="main-body">
		<form method="post" action="productDelete.php">
		<div id= "header">
			<div style="width: 85%;"> <h1>Product List</h1></div >
			<div><button type="button" onclick = "location.href='productAdd.php';">ADD</button></div>
			<div><button class = "right-border-button" type = "submit" name = "delete" id = "delete-product-btn">MASS DELETE</button></div>
		</div>
		<div id="list">
			<?php
				spl_autoload_register();
				$products = new Models\DbProducts();
				$products->setProducts();
				$products->showProducts();
            ?>			
		</div>
		<div id = "footer">
			<p> Scandiweb Test assignment</p>
		</div>
		</form>
	</div>
</body>
</html>