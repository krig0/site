<?php
namespace Models;

class Database{
    private $mysqli='';

    public function __construct()
    {
        $this->mysqli = new \mysqli('localhost', 'root', 'qwerty', 'scandiwebtest');
        $this->mysqli->query("CREATE TABLE IF NOT EXISTS products (
            id int AUTO_INCREMENT PRIMARY KEY,
            sku char(255) NOT NULL,
            name char(255) NOT NULL,
            price float NOT NULL,
            size char(255),
            weight char(255),
            height char(255),
            width char(255),
            length char(255));");
    }

    public function insert($values)
    {
        $tableColumns = implode(',', array_keys($values));
        $tableValue = implode("','", $values);

        $sql="INSERT INTO products ($tableColumns) VALUES('$tableValue')";
        $this->mysqli->query($sql);
    }

    public function delete($id)
    {
        $sql= "DELETE FROM products WHERE id=".$id;
        $this->mysqli->query($sql);
    }

    public function select($sql){ return $this->mysqli->query($sql);}
    }
?>