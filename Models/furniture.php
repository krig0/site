<?php
namespace Models;

class Furniture extends Product{
    private $height;
    private $width;
    private $length;
    function __construct($array) {
        parent::__construct($array);
        $this->height = $array->height;
        $this->width = $array->width;
        $this->length = $array->length;
    }
    public function buildHtml(){
        parent::buildHtml();
        echo "<p> Dimension ".$this->height.'x'.$this->width.'x'.$this->length."</p>";
        echo "</div>";
    }
}
?>