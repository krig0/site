<?php
namespace Models;

class Dvd extends Product{
    private $size;
    function __construct($array) {
        parent::__construct($array);
        $this->size = $array->size;
    }
    public function buildHtml(){
        parent::buildHtml();
        echo "<p> Size ".$this->size." Mb </p>";
        echo "</div>";
    }
}
?>