<?php
namespace Models;

abstract class Product{
    protected $id;
    protected $sku;
    protected $name;
    protected $price;
    function __construct($array) {
        $this->id = $array->id;
        $this->sku = $array->sku;
        $this->name = $array->name;
        $this->price = $array->price;
    }
    public function buildHtml(){
        echo "<div class = 'product-info'>";
        echo "<input type = 'checkbox' class = 'delete-checkbox' name = 'checkbox[]' value = '".$this->id."'></input>";
        echo "<p>".$this->sku."</p>";
        echo "<p>".$this->name."</p>";
        echo "<p>".$this->price."$ </p>";
    }
}

?>