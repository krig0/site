<?php
namespace Models;

class DbProducts{
    private $productValues = array();
    private $idArray =  array();
    private $showingProducts = array();

    function setProductValues($formValues)
    { 
        $this->setValue($formValues,"sku");
        $this->setValue($formValues,"name");
        $this->setValue($formValues,"price");
        $this->setValue($formValues,"size");
        $this->setValue($formValues,"weight");
        $this->setValue($formValues,"height");
        $this->setValue($formValues,"width");
        $this->setValue($formValues,"length");
    }
    function setValue($formValues,$valueName){ $this->productValues[$valueName] = $formValues[$valueName];}
    function saveProduct()
    {
        $db = new Database();
        $db->insert($this->productValues);
        header('location: index.php');
    }
    
    function setIds(){ foreach($_POST['checkbox'] as $id) array_push($this->idArray,$id);}
    function deleteProducts()
    {
        require_once "database.php";
        $db = new Database;
        foreach($this->idArray as $id) $db->delete($id);
        header('location: index.php');
    }

    function setProducts()
    {   
        $db  = new Database(); 
        $booksSql = "SELECT id, sku, name, price, weight FROM products where weight <>"."''";
        $dvdsSql = "SELECT id, sku, name, price, size FROM products where size <>"."''";
        $furnitureSql = "SELECT id, sku, name, price, height, width, length FROM products where height <>"."''";

        $showingProducts = $db->select($booksSql);
        while ($book = $showingProducts->fetch_object()) {
            $this->showingProducts[$book->id] = new Book($book);
        }

        $showingProducts = $db->select($dvdsSql);
        while ($dvd =  $showingProducts->fetch_object()) {
            $this->showingProducts[$dvd->id] = new Dvd($dvd);
        }

        $showingProducts = $db->select($furnitureSql);
        while ($furniture =  $showingProducts->fetch_object()) {
            $this->showingProducts[$furniture->id] = new Furniture($furniture);
        }
    }
    function showProducts()
    { 
        ksort($this->showingProducts);
        foreach($this->showingProducts as $product) $product->buildHtml(); 
        echo "<input type = 'checkbox' class = 'fake-checkbox' name = 'checkbox[]' value = 'true' checked ></input>";
    }
}
?>