<?php
namespace Models;

class Book extends Product{
    private $weight;
    function __construct($array) {
        parent::__construct($array);
        $this->weight = $array->weight;
    }
    public function buildHtml(){
        parent::buildHtml();
        echo "<p> Weight ".$this->weight." Kg </p>";
        echo "</div>";
    }
}
?>