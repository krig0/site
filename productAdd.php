<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Add product</title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<form method="post" action="" onsubmit="return formRedirect();" name = "productForm" id = "product_form">
		<div id= "header">
			<div style="width: 25%;"><h1>Product Add</h1></div>
			<div id = "alert-td"><span id = "alert"></span></div>
			<div><button type="submit" name="save" >Save</button></div>
			<div><button type="button" class  = "right-border-button" name="cancel" onclick = "location.href='index.php';">Cancel</button></div>
		</div>
  		<div>
		  	<label class = "label">Sku</label>
			<input name="sku" id = "sku">
 		</div>
		<div>
		 	<label class = "label">Name</label>
			<input name="name" id = "name">
 		</div>
		<div>
		 	<label class = "label">Price ($)</label>
			<input name="price" id = "price">
 		</div>
		 <div>
			<label>Type switcher<label>
			<select onchange="showText(this.value);" name = "param_name" id = "productType">
				<option value = "noChoose">Choose type</option>
				<option value = "dvd" id = "DVD">DVD</option>
				<option value = "book" id = "Book">Book</option>
				<option value = "furniture" id = "Furniture">Furniture</option>
			</select>
		</div>
		<div id = "dvd-field">
			<label class = "label"> Size(MB) </label>
			<input name="size" id = "size">
		</div>
		<div id = "book-field">
			<label class = "label"> Weight(KG) </label>
			<input name="weight" id = "weight">
		</div>
		<div id = "furniture-field">
			<div>
				<label class = "label"> Height(CM) </label>
				<input name="height" id = "height">
			</div>
			<div>
				<label class = "label"> Width(CM) </label>
				<input name="width" id = "width">
			</div>
			<div>
				<label class = "label"> Length(CM) </label>
				<input name="length" id = "length">
			</div>
		</div>
		<strong id = "descript"></strong>
	</form>

	<div id = "footer">
			<p> Scandiweb Test assignment</p>
	</div>
</body>
<script src = "script.js"></script>
</html>